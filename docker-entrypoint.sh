#!/bin/bash
set -e

case "$1" in
    start)
        python3 manage.py migrate --noinput
        uwsgi --ini uwsgi.ini
        ;;
    config)
        python3 manage.py migrate --noinput
        ;;
    tests)
        flake8
        python3 manage.py test
        ;;
    *)
        exec "$@"
esac