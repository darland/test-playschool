from rest_framework import serializers

from . import models


class KidSerializer(serializers.ModelSerializer):
    photo = serializers.ImageField(max_length=None, use_url=True, required=False)

    class Meta:
        model = models.Kid
        fields = (
            'id', 'first_name', 'last_name', 'gender', 'photo', 'birthday', 'group', 'status'
        )

    def create(self, validated_data):
        return models.Kid.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.photo = validated_data.get('photo', instance.photo)
        instance.birthday = validated_data.get('birthday', instance.birthday)
        instance.group = validated_data.get('group', instance.group)
        instance.status = validated_data.get('status', instance.status)
        instance.save()
        return instance


class JournalSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Journal
        fields = (
            'kid', 'parent', 'date'
        )

    def create(self, validated_data):
        return models.Journal.objects.create(**validated_data)
