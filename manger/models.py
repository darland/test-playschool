from django.db import models
from django.db.models import Case, Value, When
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from .utils import unique_filename

GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
)
PARENT_CHOICES = (
    ('f', 'Father'),
    ('m', 'Mother'),
)


class Kid(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    photo = models.ImageField(upload_to=unique_filename('avatars'))
    birthday = models.DateField(null=False, blank=False)
    group = models.CharField(max_length=3)
    status = models.BooleanField()


class Journal(models.Model):
    kid = models.ForeignKey(
        Kid, related_name='quotas_volumes', on_delete=models.CASCADE
    )
    date = models.DateTimeField(default=timezone.now)
    parent = models.CharField(max_length=1, choices=PARENT_CHOICES)


@receiver(post_save, sender=Journal)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Kid.objects.filter(id=instance.kid.id).update(
            status=Case(
                When(status=True, then=Value(False)),
                When(status=False, then=Value(True)),
            )
        )
