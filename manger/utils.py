import uuid


def unique_filename(folder=None):
    def w_unique_filename(instance, filename):
        ext = filename.split('.').pop()
        filename = f"{uuid.uuid4()}.{ext}"
        if folder is not None:
            filename = f"{folder}/{filename}"
        return filename
    return w_unique_filename
