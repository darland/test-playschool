from rest_framework import generics
from rest_framework.views import APIView

from . import models, serializers


class KidListView(generics.CreateAPIView):
    queryset = models.Kid.objects.all()
    serializer_class = serializers.KidSerializer


class KidView(generics.RetrieveUpdateAPIView):
    queryset = models.Kid.objects.all()
    serializer_class = serializers.KidSerializer


class JournalCreateView(generics.CreateAPIView):
    queryset = models.Journal.objects.all()
    serializer_class = serializers.JournalSerializer


class KidsInSchoolListView(generics.ListAPIView):
    queryset = models.Kid.objects.all().filter(status=True)
    serializer_class = serializers.KidSerializer


class ManageJournalView(APIView):
    VIEWS_BY_METHOD = {
        'GET': KidsInSchoolListView.as_view,
        'POST': JournalCreateView.as_view
    }

    def dispatch(self, request, *args, **kwargs):
        if request.method in self.VIEWS_BY_METHOD:
            return self.VIEWS_BY_METHOD[request.method]()(request, *args, **kwargs)

        return super().dispatch(request, *args, **kwargs)
