from copy import copy
from datetime import date
from rest_framework import status
from rest_framework.test import APITestCase

from ..models import Kid
from ..serializers import KidSerializer


def remove_photo(r, s):
    r.data.pop('photo', None)
    s.fields.pop('photo', None)
    return r, s


class KidTest(APITestCase):
    def setUp(self):
        self.bob = Kid.objects.create(
            first_name='Bob', last_name='Ghost', gender='M',
            birthday=date.today(), photo='ava250.png', group='1s', status=False
        )
        self.alias = Kid.objects.create(
            first_name='Alias', last_name='Tri', gender='M',
            birthday=date.today(), photo='ava1.jpg', group='1s', status=False
        )

    def test_get_all_kids(self):
        response = self.client.get('/api/kids/')
        self.assertEqual(
            response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED
        )

    def test_get_one_kid(self):
        response = self.client.get(f'/api/kids/{self.bob.pk}/')
        bob = Kid.objects.get(pk=self.bob.pk)
        serializer = copy(KidSerializer(bob))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response, serializer = remove_photo(response, serializer)

        self.assertEqual(response.data, serializer.data)

    def test_update_kid(self):
        response = self.client.patch(
            f'/api/kids/{self.bob.pk}/', {'first_name': 'Bob1'}, format='json'
        )
        bob = Kid.objects.get(pk=self.bob.pk)
        serializer = copy(KidSerializer(bob))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response, serializer = remove_photo(response, serializer)

        self.assertEqual(response.data, serializer.data)

    def test_journal_empty_playschool(self):
        response = self.client.get('/api/journal/')
        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(response.data, [])

    def test_journal_one_bob(self):
        response = self.client.patch(
            f'/api/journal/', {'kid': self.bob.pk, 'parent': 'm'}, format='json'
        )
        self.assertEqual(
            response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED
        )

        self.client.post(
            f'/api/journal/', {'kid': self.bob.pk, 'parent': 'm'}, format='json'
        )
        response = self.client.get('/api/journal/')

        self.assertEqual(len(response.data), 1)

        self.client.post(
            f'/api/journal/', {'kid': self.bob.pk, 'parent': 'm'}, format='json'
        )
        response = self.client.get('/api/journal/')

        self.assertEqual(len(response.data), 0)
