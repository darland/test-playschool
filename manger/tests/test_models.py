from datetime import date
from django.test import TestCase

from ..models import Kid


class KidTest(TestCase):
    def setUp(self):
        Kid.objects.create(
            first_name='Casper', last_name='Ghost', gender='M',
            birthday=date.today(), photo='ava250.png', group='1s', status=False
        )
        Kid.objects.create(
            first_name='Alis', last_name='Tri', gender='M',
            birthday=date.today(), photo='ava1.jpg', group='1s', status=False
        )
