FROM python:3.6
WORKDIR /code

EXPOSE 8080 5555
ENTRYPOINT ["/code/docker-entrypoint.sh"]
CMD ["start"]
COPY requirements.txt /code/requirements.txt
RUN pip3 install -r requirements.txt
COPY . /code